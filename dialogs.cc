/*

SPDX-License-Identifier: GPL-3.0-or-later

Copyright (C) 2019 Mike Miller

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <QAbstractButton>
#include <QDialogButtonBox>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include "dialogs.h"
#include <iostream>

example_file_dialog::example_file_dialog (QWidget *parent)
  : QFileDialog (parent)
{
  connect (this, &QFileDialog::accepted, this, &example_file_dialog::forward_selected_file);
}

void example_file_dialog::forward_selected_file ()
{
  QStringList files = selectedFiles ();
  QString dir_name = directory ().absolutePath ();
  QString file_name = QFileInfo (files[0]).fileName ();
  emit file_and_directory_selected (file_name, dir_name);
}

example_main_dialog::example_main_dialog ()
  : QDialog ()
{
  QDialogButtonBox *box = new QDialogButtonBox;
  button1 = box->addButton (tr ("Default File Dialog (System Native)"), QDialogButtonBox::ActionRole);
  button2 = box->addButton (tr ("Qt Widget-Based File Dialog"), QDialogButtonBox::ActionRole);
  connect (box, &QDialogButtonBox::clicked, this, &example_main_dialog::clicked);

  QLabel *label = new QLabel (tr ("Choose a file dialog implementation:"));

  QVBoxLayout *layout = new QVBoxLayout;
  layout->addWidget (label);
  layout->addWidget (box);
  setLayout (layout);

  setWindowTitle (tr ("Qt File Dialog Bug"));
}

void example_main_dialog::clicked (QAbstractButton *button)
{
  example_file_dialog *dialog = new example_file_dialog (this);
  if (button == button2)
    dialog->setOption (QFileDialog::DontUseNativeDialog);
  connect (dialog, &example_file_dialog::file_and_directory_selected,
           this, &example_main_dialog::file_and_directory_selected);
  dialog->show ();
}

void example_main_dialog::file_and_directory_selected (const QString& file, const QString& directory)
{
  std::cout << "A file was selected:" << std::endl;
  std::cout << "  directory: " << directory.toStdString () << std::endl;
  std::cout << "  file:      " << file.toStdString () << std::endl;
  std::cout << std::endl;
}
