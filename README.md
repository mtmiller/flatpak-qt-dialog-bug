Flatpak Qt Dialog Bug
=====================

Trivial project demonstrating a bug with QDialog running in a Flatpak
container.

## Details

When a Qt application uses standard system native file dialogs in a Flatpak
container, the directory returned by the file dialog doesn't agree with the
directory that was selected by the user. The same program outside of Flatpak
returns the correct directory. Qt widget-based file dialogs work correctly
either inside or outside of Flatpak.

I suspect this is a bug in the Flatpak portal that instantiates a native file
dialog and returns the results over D-Bus to the contained Qt application.

## Usage

Build and run this example program normally with

    qmake
    make
    ./flatpak-qt-dialog-bug

Build and run this example program in Flatpak with

    flatpak-builder --repo=repo build io.gitlab.mtmiller.flatpak-qt-dialog-bug.yaml
    flatpak build-bundle repo flatpak-qt-dialog-bug.flatpak io.gitlab.mtmiller.flatpak-qt-dialog-bug
    flatpak install [--user] flatpak-qt-dialog-bug.flatpak
    flatpak run io.gitlab.mtmiller.flatpak-qt-dialog-bug

Navigate to a different directory from the home or current working directory.
