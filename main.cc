/*

SPDX-License-Identifier: GPL-3.0-or-later

Copyright (C) 2019 Mike Miller

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <QApplication>
#include "dialogs.h"

int main (int argc, char *argv[])
{
  QApplication app (argc, argv);

  QApplication::setApplicationName ("flatpak-qt-dialog-bug");
  QGuiApplication::setDesktopFileName ("io.gitlab.mtmiller.flatpak-qt-dialog-bug");

  example_main_dialog dialog;
  dialog.show ();

  return app.exec ();
}
