/*

SPDX-License-Identifier: GPL-3.0-or-later

Copyright (C) 2019 Mike Miller

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#if ! defined (example_dialog_h)
#define example_dialog_h

#include <QDialog>
#include <QFileDialog>

class QAbstractButton;

class example_file_dialog : public QFileDialog
{
  Q_OBJECT
public:
  explicit example_file_dialog (QWidget *parent = nullptr);
  virtual ~example_file_dialog () {}
private slots:
  void forward_selected_file ();
signals:
  void file_and_directory_selected (const QString& file, const QString& directory);
};

class example_main_dialog : public QDialog
{
  Q_OBJECT
public:
  explicit example_main_dialog ();
  virtual ~example_main_dialog () {}
private slots:
  void clicked (QAbstractButton *button);
  void file_and_directory_selected (const QString& file, const QString& directory);
private:
  QAbstractButton *button1;
  QAbstractButton *button2;
};

#endif
